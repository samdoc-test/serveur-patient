# Serveur-patient

Ce serveur permet d'avoir une api qui donne une liste de patient stocké sur une base donnée ou d'obtenir des informations précise sur un patient.

## Dépendances
Le serveur est écris en java avec l'api vert.x 4 (pour le serveur), Jackson Databind (pour l'encodage en JSON) ainsi que le connecteur Java fourni par mariadb pour la communication avec la base de donnée.
Le serveur est compatible avec Java 8 mais Java 11 est fortement recommandé.
Il est possible d'adapter facilement l'application pour d'autres type de base de donnée.

## Sécurité
Le serveur utilise un certificat auto-signé et prend en charge la communication en HTTPS. Toute connexion en HTTP sera redirigé vers une connexion en HTTPS. Le serveur est compatible avec TLS 1.2 et 1.3 uniquement.

Lors du démarrage du serveur, il vous saura demandé d'entrer l'adresse du serveur de base de donné, un nom d'utilisateur et un mot de passe.
Cela permet d'éviter d'avoir le couple identifiant/mot de passe dans le code de l'application.

Vous pouvez retrouver un utilisateur exemple sur le projet dédié à la base de donnée.

## Api
Chaque API donne un fichier JSON comme résultat.

L'api qui permet d'accéder à la liste des patients est accessible depuis l'adresse `https://localhost:8070/api/patient` en utilisant la méthode get ou post. Si vous le souhaitez, vous pouvez filtrer les résultat avec les paramètres (query) id, nom, prenom ou age.

L'api qui permet d'accéder à des informations précises sur un patient est accessible depuis l'adresse `https://localhost:8070/api/patient/idDuPatient` en utilisant la méthode get ou post.

Pour la liste des patient, une actualisation est éffectué toute les 5 minutes pour éviter de faire une requête à chaque appel de l'API.

## Contexte
Ce projet est un test technique demandé par samdoc pour ma demande de stage. Ce projet utilise une base de donnée que vous pouvez trouvez sur la page du groupe.

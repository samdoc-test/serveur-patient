package serveurpatient;

import java.util.HashMap;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.MultiMap;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.Json;
import io.vertx.core.net.SelfSignedCertificate;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import serveurpatient.donnees.Patient;
import serveurpatient.donnees.PatientService;

public class HttpsServerVerticule extends AbstractVerticle{
	
	private static final String contentType = "content-type";
	private static final String contentTypeJSON = "application/json; charset=utf-8";
	private static final String notFound = "Not found";
	private static final String incorrectValue = "Incorrect value";
	private static final String apiPath = "/api/patient";
	private static final String homePath = "/";
	private static final String assets = "assets/";
	private static final String assetsParams = "/assets/:fileName";
	private static final String fileName = "fileName";
	private static final String paramsId = "/:id";
	private static final String id = "id";
	//private OAuth2Auth oauth2;
	
	private PatientService servicePatient;
	public void start() {
		try {
		servicePatient = MainServeur.getServicePatient();
		final Router router = Router.router(vertx);
		System.out.println("Création de l'handler "+homePath);
		
		//Cela ne semble pas marcher en Localhost
		/*oauth2 = GitLabAuth.create(vertx, "fb70bcf84c267573369d66d109d49f6415e6c625e11ea3d02893556cceadeab7", "0366b7f2c0d35ae86619877ad54803f7fd22e5880a0cd523e1e3cdbb62726f48");
		String authorization_uri = oauth2.authorizeURL(new JsonObject()
			      .put("redirect_uri", "https://localhost:8070/callback")
			      .put("scope", "notifications")
			      .put("state", "3(#0/!~"));
			    String code = "xxxxxxxxxxxxxxxxxxxxxxxx";

			    oauth2.authenticate(
			      new JsonObject()
			        .put("code", code)
			        .put("redirect_uri", "https://localhost:8070/callback"))
			      .onSuccess(user -> {
			        System.out.println("Yes");
			      })
			      .onFailure(err -> {
				        System.out.println("No");
			      });
		
		router.route(homePath).handler(routingContext -> {
			HttpServerResponse response = routingContext.response();
			response.putHeader("Location", authorization_uri)
		      .setStatusCode(302)
		      .end();
		});*/
		
		System.out.println("Création de l'handler "+apiPath);
		router.get(apiPath).handler(this::getAllPatient);
		router.post(apiPath).handler(this::getAllPatient);
		System.out.println("Création de l'handler "+apiPath+paramsId);
		router.get(apiPath+paramsId).handler(this::getPatient);
		router.post(apiPath+paramsId).handler(this::getPatient);
		

		router.route(assetsParams).handler(routingContext -> {
	          routingContext.response().sendFile(assets+routingContext.request().params().get(fileName));
	      });
		System.out.println("Création du certificat TLS");
		SelfSignedCertificate certificate = SelfSignedCertificate.create();
		HttpServerOptions secureOptions = new HttpServerOptions().
				setTcpKeepAlive(true).
				setSsl(true).
				removeEnabledSecureTransportProtocol("TLSv1").
				removeEnabledSecureTransportProtocol("TLSv1.1").
				addEnabledSecureTransportProtocol("TLSv1.2").
				addEnabledSecureTransportProtocol("TLSv1.3").
				setKeyCertOptions(certificate.keyCertOptions()).
				setTrustOptions(certificate.trustOptions());
		
		
		System.out.println("Lancement de l'écoute sur le port "+MainServeur.port);
		vertx.createHttpServer(secureOptions).requestHandler(router).listen(config().getInteger("http.port", MainServeur.port));

		System.out.println("Serveur lancé");
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	public void stop() throws Exception {
		servicePatient.close();
		System.out.println("Serveur arrété");
	}
	private void getPatient(final RoutingContext routingContext) {
		try {
			routingContext.response()
			.putHeader(contentType, contentTypeJSON)
			.end(Json.encode(servicePatient.findById(Integer.parseInt(routingContext.request().params().get(id)))));
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	private void getAllPatient(final RoutingContext routingContext) {
		try{
			Object resultat;
			MultiMap t = routingContext.request().params();
			if(t.isEmpty())
				resultat = servicePatient.findAll();
			else if(t.size()==1&&t.contains(id)) {
				try {
					resultat = servicePatient.findById(Integer.parseInt(t.get(id)));
				}catch(NumberFormatException e) {
					resultat = incorrectValue;
				}
			}
			else {
				HashMap<Integer, Patient> hm = servicePatient.find(t);
				if(hm==null)
					resultat = incorrectValue;
				else if(hm.isEmpty())
					resultat = notFound;
				else
					resultat = hm;
			}
			routingContext.response()
			.putHeader(contentType, contentTypeJSON)
			.end(Json.encode(resultat));
		}catch(Exception e) {
			e.printStackTrace();
		}
	}

}

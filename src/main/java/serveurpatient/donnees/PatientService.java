package serveurpatient.donnees;

import java.io.Console;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Scanner;

import io.vertx.core.MultiMap;
import serveurpatient.domaine.Mariadb_BDD;

public class PatientService {
	private HashMap<Integer, Patient> list = new HashMap<Integer, Patient>();
	private final Mariadb_BDD connexion;
	private final Thread actualisation;
	private boolean stop = false;
	//En milliseconde
	private static final long actualisationTime = 300000;

	public PatientService() {
		boolean connecter = false;
		String login, mdp;
		Console console = System.console();
		Mariadb_BDD tempConnexion = null;
		if(console==null)
			System.err.println("Impossible d'ouvrir la console, pour une meilleur sécurité. Cela peut arriver si vous executez le programme sous eclipse");
	    System.out.println("Veuillez rentrer l'adresse de la base de données (laissez vide pour localhost)");
		Scanner sc = new Scanner(System.in);
	    String adresse = sc.nextLine();
	    if(adresse==null||adresse.isEmpty())
	    	adresse="localhost";
		do {
			try {
				System.out.println("Veuillez rentrer votre identifiant pour vous connecter à la base de données");
				login = sc.nextLine();
				System.out.println("Veuillez rentrer votre mot de passe pour vous connecter à la base de données");
				if(console!=null)
					mdp = new String(console.readPassword());
				else
					mdp = sc.nextLine();
				tempConnexion  = new Mariadb_BDD(adresse, login, mdp);
				connecter=true;
			}catch(SQLException e) {
				System.out.println("Erreur lors de la connexion "+e);
			}
		}while(!connecter);
		connexion = tempConnexion;
		sc.close();
		actualisation = new Thread(() -> {
			try {
				while(!stop) {
					initList();
					Thread.sleep(actualisationTime);
				}
			}catch(InterruptedException e) {
				
			}
			
		}); 
		actualisation.start();
	}
	public void close() {
		stop = true;
		actualisation.interrupt();
	}
	public HashMap<Integer, Patient> findAll() {
		return list;
	}
	public HashMap<Integer, Patient> find(MultiMap multimap) {
		return connexion.getPatient(multimap);
	}
	public Patient findById(final int id) {
		return connexion.addMaladieAttrapee(list.get(id));
	}
	public HashMap<Integer, Patient> findByName(HashMap<Integer, Patient> liste, final String nom) {
		HashMap<Integer, Patient> tempList = new HashMap<Integer, Patient>();
		Patient patient;
		for(int i: liste.keySet()) {
			patient = liste.get(i);
			if(patient.getNom().toLowerCase().equals(nom))
				tempList.put(i,patient);
		}
		return tempList;
	}
	public HashMap<Integer, Patient> findBySurname(HashMap<Integer, Patient> liste, final String prenom) {
		HashMap<Integer, Patient> tempList = new HashMap<Integer, Patient>();
		Patient patient;
		for(int i: liste.keySet()) {
			patient = liste.get(i);
			if(patient.getPrenom().toLowerCase().equals(prenom))
				tempList.put(i,patient);
		}
		return tempList;
	}
	public HashMap<Integer, Patient> findByAge(HashMap<Integer, Patient> liste, final int age) {
		HashMap<Integer, Patient> tempList = new HashMap<Integer, Patient>();
		Patient patient;
		for(int i: liste.keySet()) {
			patient = liste.get(i);
			if(patient.getAge()==age)
				tempList.put(i,patient);
		}
		return tempList;
	}
	private final void initList() {
		list = connexion.getAllPatient();
		/*final Patient cb =new Patient(0,"Christophe","Bernard",54);
		final Patient pb = new Patient(1,"Pierre","Bernard",72);
		final Patient oc = new Patient(2,"Oswald","Clara",35);
		final Patient pa = new Patient(3,"Pond","Amy",37);
		final Patient td = new Patient(4,"Tennant","David",42);
		final Patient tl = new Patient(5,"Torvald","Linus",40);
		// Ajouts dans la map
		list.put(cb.getId(), cb);
		list.put(pb.getId(), pb);
		list.put(oc.getId(), oc);
		list.put(pa.getId(), pa);
		list.put(td.getId(), td);
		list.put(tl.getId(), tl);*/
	}
	public Patient getPatientMaladies(HashMap<Integer, Patient> hm) {
		Patient temp = ((Patient) hm.values().toArray()[0]).clone();
		temp = connexion.addMaladieAttrapee(temp);
		return temp;
	}
}

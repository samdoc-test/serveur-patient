package serveurpatient.donnees;

public class Patient {
	private final int id;
	private final String nom;
	private final String prenom;
	private final int age;
	private final char genre;
	public Patient(int id, String nom, String prenom, int age, char genre) {
		this.id=id;
		this.age=age;
		this.nom=nom;
		this.prenom=prenom;
		this.genre=genre;
	}
	public int getId() {
		return id;
	}
	public int getAge() {
		return age;
	}
	public String getNom() {
		return nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public char getGenre() {
		return genre;
	}
	public Patient clone() {
		return new Patient(id, nom, prenom, age, genre);
	}
}

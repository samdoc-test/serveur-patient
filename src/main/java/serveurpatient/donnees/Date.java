package serveurpatient.donnees;

import java.time.LocalDate;

public class Date {
	
	private final LocalDate date;

	public Date(LocalDate date) {
		this.date=date;
	}
	@Override
	public String toString() {
		return date.getDayOfMonth()+"/"+date.getMonthValue()+"/"+date.getYear();
	}
}

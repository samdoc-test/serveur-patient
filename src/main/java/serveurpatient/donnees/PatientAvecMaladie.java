package serveurpatient.donnees;

import java.util.HashMap;

public class PatientAvecMaladie extends Patient {

	private HashMap<Date, MaladieAttrape> maladies;
	public PatientAvecMaladie(int id, String nom, String prenom, int age, char genre) {
		super(id, nom, prenom, age, genre);
		maladies = new HashMap<Date, MaladieAttrape>();
	}
	public PatientAvecMaladie(int id, String nom, String prenom, int age, char genre, HashMap<Date, MaladieAttrape> maladies) {
		super(id, nom, prenom, age, genre);
		this.maladies=maladies;
	}
	public HashMap<Date, MaladieAttrape> getMaladies(){
		return maladies;
	}
	public void clearMaladie() {
		maladies.clear();
	}
	public void addMaladie(Date date, int idMaladie, String nom, int idMedicament, String medicament) {
		maladies.put(date, new MaladieAttrape(idMaladie, idMedicament, nom, medicament));
	}

}

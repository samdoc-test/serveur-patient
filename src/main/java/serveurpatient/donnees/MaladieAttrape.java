package serveurpatient.donnees;

public class MaladieAttrape {
	private final int idMaladie;
	private final int idMedicament;
	private final String nomMaladie;
	private final String nomMedicament;
	public MaladieAttrape(int idMaladie, int idMedicament, final String nomMaladie, final String nomMedicament) {
		this.idMaladie=idMaladie;
		this.idMedicament=idMedicament;
		this.nomMaladie=nomMaladie;
		this.nomMedicament=nomMedicament;
	}
	public String getNomMaladie() {
		return nomMaladie;
	}
	public String getNomMedicament() {
		return nomMedicament;
	}
	public int getIdMedicament() {
		return idMedicament;
	}
	public int getIdMaladie() {
		return idMaladie;
	}
}

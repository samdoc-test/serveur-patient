package serveurpatient;

import io.vertx.core.Vertx;

public class Main {
	public static void main(String[] args) throws Exception {
		System.out.println("Demarrage du serveur");
	    final Vertx vertx = Vertx.vertx();
	    vertx.deployVerticle(new MainServeur());
	}
}

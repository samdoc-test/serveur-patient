package serveurpatient.domaine;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

import io.vertx.core.MultiMap;
import serveurpatient.donnees.Date;
import serveurpatient.donnees.Patient;
import serveurpatient.donnees.PatientAvecMaladie;

public abstract class BDD {
	
    private ResultSet curseurJava;
    private Statement objReq;
    private final String chaineConnexion;
    private final String login;
    private final String mdPasse;
    private Connection conn;
    private final String pilote;
    
	private static final String id = "id";
	private static final String PA_ID = "PA_ID";
	private static final String PA_NOM = "upper(PA_NOM)";
	private static final String PA_PRENOM = "upper(PA_PRENOM)";
	private static final String PA_GENRE = "PA_GENRE";
	private static final String nom = "nom";
	private static final String prenom = "prenom";
	private static final String age = "age";
	private static final String genre = "genre";
	private static final String requete = "select PA_ID, PA_NOM, PA_PRENOM, floor(DATEDIFF(sysdate(), PA_DATE_NAISSANCE)/365) as age, PA_GENRE from patient";
	private static final String where = " where ";
	private static final String and = " and ";
	private static final char equal = '=';
	private static final String like = " like '";
	private static final String getAge = "floor(DATEDIFF(sysdate(), PA_DATE_NAISSANCE)/365)";
	
	
	public BDD(String login, String mdPasse, String pilote, String chaineConnexion) {
		this.login=login;
		this.mdPasse=mdPasse;
		this.pilote=pilote;
		this.chaineConnexion=chaineConnexion;
	}
	public HashMap<Integer, Patient> getAllPatient() {
		HashMap<Integer, Patient> liste=new HashMap<Integer, Patient>();
        System.out.println("Actualisation de la liste des patients");
        String req = requete;
    	try {
    		curseurJava = objReq.executeQuery(req);
	    	while (curseurJava.next()) {
	    		Patient p = new Patient(curseurJava.getInt(1), curseurJava.getString(2) , curseurJava.getString(3), curseurJava.getInt(4), curseurJava.getString(5).charAt(0));
                liste.put(p.getId(), p);
	    	}
	        return liste;
    	}
    	catch(SQLException ex) {
	    	System.err.println (ex.getErrorCode());
	    	System.err.println (ex.getMessage());
	    	System.err.println("Erreur : "+ex);
	    	return null;
    	}
    }public Patient addMaladieAttrapee(Patient patient){
    	PatientAvecMaladie temp = new PatientAvecMaladie(patient.getId(), patient.getNom(), patient.getPrenom(), patient.getAge(), patient.getGenre());
		final String req = "select AT_DATE, MA_ID, MA_NOM, ME_ID, ME_NOM from MALADIE_ATTRAPEE join PATIENT using(PA_ID) join MALADIE using(MA_ID) left join MEDICAMENT using(ME_ID) where PA_ID="+patient.getId();
		try {
    		curseurJava = objReq.executeQuery(req);
	    	while (curseurJava.next()) {
	    		temp.addMaladie(new Date(curseurJava.getDate(1).toLocalDate()), curseurJava.getInt(2), curseurJava.getString(3), curseurJava.getInt(4), curseurJava.getString(5));
	    	}
	    	return temp;
    	}
    	catch(SQLException ex) {
	    	System.err.println (ex.getErrorCode());
	    	System.err.println (ex.getMessage());
	    	System.err.println("Erreur : "+ex);
	    	return patient;
    	}
	}
    @Deprecated
	public HashMap<Integer, PatientAvecMaladie> addMaladieAttrapee(HashMap<Integer, PatientAvecMaladie> liste){
		HashMap<Integer, PatientAvecMaladie> hashMap = liste;
		final String req = "select PA_ID, AT_DATE, MA_ID, MA_NOM, ME_ID, ME_NOM from MALADIE_ATTRAPEE join PATIENT using(PA_ID) join MALADIE using(MA_ID) left join MEDICAMENT using(ME_ID)";
		try {
    		curseurJava = objReq.executeQuery(req);
	    	while (curseurJava.next()) {
	    		int tempid = curseurJava.getInt(1);
	    		hashMap.get(tempid).addMaladie(new Date(curseurJava.getDate(2).toLocalDate()), curseurJava.getInt(3), curseurJava.getString(4), curseurJava.getInt(5), curseurJava.getString(6));
	    	}
	    	return hashMap;
    	}
    	catch(SQLException ex) {
	    	System.err.println (ex.getErrorCode());
	    	System.err.println (ex.getMessage());
	    	System.err.println("Erreur : "+ex);
	    	return liste;
    	}
	}
	public HashMap<Integer, Patient> getPatient(MultiMap multimap) {
		HashMap<Integer, Patient> liste=new HashMap<Integer, Patient>();
        StringBuilder sb = new StringBuilder(requete);
        boolean addAnd=false;
        if(multimap.size()>0) {
        	if(multimap.contains(id)) {
            	if(addAnd)
            		sb.append(and);
            	else {
                	sb.append(where);
                	addAnd=true;
            	}
            	sb.append(PA_ID);
            	sb.append(equal);
            	sb.append(multimap.get(id));
            }
            if(multimap.contains(nom)) {
            	if(addAnd)
            		sb.append(and);
            	else {
                	sb.append(where);
                	addAnd=true;
            	}
            	sb.append(PA_NOM);
            	sb.append(like);
            	sb.append(multimap.get(nom).toUpperCase());
            	sb.append('\'');
            }
            if(multimap.contains(prenom)) {
            	if(addAnd)
            		sb.append(and);
            	else {
                	sb.append(where);
                	addAnd=true;
            	}
            	sb.append(PA_PRENOM);
            	sb.append(like);
            	sb.append(multimap.get(prenom).toUpperCase());
            	sb.append('\'');
            }
            if(multimap.contains(age)) {
            	if(addAnd)
            		sb.append(and);
            	else
                	sb.append(where);
            	sb.append(getAge);
            	sb.append(equal);
            	sb.append(multimap.get(age));
            }
            if(multimap.contains(genre)) {
            	if(addAnd)
            		sb.append(and);
            	else
                	sb.append(where);
            	sb.append(PA_GENRE);
            	sb.append(equal);
            	sb.append("\'"+multimap.get(genre)+"\'");
            }
        }
    	try {
    		curseurJava = objReq.executeQuery(sb.toString());
	    	while (curseurJava.next()) {
	    		Patient p = new Patient(curseurJava.getInt(1), curseurJava.getString(2) , curseurJava.getString(3), curseurJava.getInt(4), curseurJava.getString(5).charAt(0));
                liste.put(p.getId(), p);
	    	}
	        return liste;
    	}
    	catch(SQLException ex) {
	    	System.err.println (ex.getErrorCode());
	    	System.err.println (ex.getMessage());
	    	System.err.println("Erreur : "+ex);
	    	return null;
    	}
    }
    public void connecter() throws SQLException{
    	try {
    		Class.forName(pilote);
			conn = DriverManager.getConnection(chaineConnexion,login,mdPasse);
			DatabaseMetaData metaData = (DatabaseMetaData) conn.getMetaData();
			System.out.println("Connexion réussie à "+metaData.getDatabaseProductName());
			objReq = conn.createStatement();
    	}catch(ClassNotFoundException e) {
    		System.err.println(" Erreur de chargement du driver :" + e);
    	}
    }
}

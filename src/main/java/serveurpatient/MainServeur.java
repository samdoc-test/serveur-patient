package serveurpatient;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.DeploymentOptions;
import serveurpatient.donnees.PatientService;

public class MainServeur extends AbstractVerticle{
	
	public static final int port = 8070;
	private static final PatientService servicePatient = new PatientService();
	
	@Override
	public void start() throws Exception{
		vertx.deployVerticle(HttpsServerVerticule.class.getCanonicalName());
		vertx.deployVerticle(HttpServerVerticule.class.getCanonicalName(), new DeploymentOptions().setInstances(1));
	}
	public static final PatientService getServicePatient() {
		return servicePatient;
	}
}
